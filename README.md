# Salam millət!

## Struktur

- Bütün decompile olunmuş fayllar **Decompiled** qovluğundadır
- **Original Jar file** qovluğunda dex2jar ilə decompile olunmuş pakterdir [JD-GUI](http://java-decompiler.github.io/) istifadə edərək baxa bilərsiniz
- Əsas paket və **AndroidManifest.xml** "Decompiled/az/gov/etabib" ünvanındadır

